<?php


/**
 * @file
 * This module allows admin to limit the number of nodes of a given type a role can create.
 * Example: Registered users can create just one photo gallery with ten images. "Class A users" can create up to five galleries, and webmaster can create unlimited.
 */
 
 /**
 * Implementation of hook_help().
 */ 
function node_limitnumber_help ($section) {
	if ($section == 'admin/help#modulename' ) {
		include_once ('node_limitnumber_abstract.inc');
		echo ($abstract);
	}
}
/**
	* Implementation of hook_menu().
	*/
function node_limitnumber_menu ($may_cache) {
	$items = array ();
	if ($may_cache) {
		$items[] = array (
			'path' => 'admin/settings/node_limitnumber',
			'title' => t('Node Limit Number'),
			'description' => t('Limit number of nodes of a given type a user can publish based on his/her roles.'),
			'callback' => 'node_limitnumber_admin',
			'access' => user_access('administer site configuration')
		);
	}
	return $items;
}

/**
 * Admin menu
 */
function node_limitnumber_admin () {
	if (arg(3) == 'delete' && arg(4)) {
		node_limitnumber_admin_delete_limit();
	}
	include_once ('node_limitnumber_abstract.inc');
	include_once ('node_limitnumber.inc');
	$output = $abstract . drupal_get_form('node_limitnumber_admin_add_form'); // Generate the "add rule" part
	$output .= drupal_get_form('node_limitnumber_admin_modify_form');// Generate the "modify rule" part
	return $output;
}
/**
	* Function for deleting a rule
	*/
function node_limitnumber_admin_delete_limit () {
	$q = "DELETE FROM {node_limitnumber_rules} WHERE id = %d";
	db_query ($q, arg(4));
	drupal_set_message(t('Limit deleted succesfully'));
	drupal_goto('admin/settings/node_limitnumber');
}

/** 
	* Validate the limit field form for the forms
	*/
function node_limitnumber_validate_limit ($item) {
	$num = $item['#value'];
	$val = (is_numeric($num) && intval($num) > 0 && ($num == intval($num))) ? true : false;
	if ($val == false) {
		form_error ($item, t('Limit must be a number.'));
	}
}

/** 
	* Validate the form for new rules
	* Essentially, this is going to check if the rule we're trying to insert exists.
	*/
function node_limitnumber_admin_add_form_validate($form, $form_values) {
	$q = "SELECT * FROM {node_limitnumber_rules} WHERE class = '%s' AND item = '%d' AND pubtype = '%s'";
	$result = db_query ($q, 'role', $form_values['role'], $form_values['pubtype']);
	$total = db_num_rows ($result);
	if ($total > 0) {
		form_set_error($form, t('This limit exists. If you want to change the limit, edit it instead of creating a new one'));
	}
}

/** 
	* Saving the form for new rules
	*/
function node_limitnumber_admin_add_form_submit ($form, $form_values) {
	$q = "INSERT INTO {node_limitnumber_rules} (class , item, pubtype, max) VALUES ('%s', '%d', '%s', '%d')";
	db_query ($q, 'role', $form_values['role'], $form_values['pubtype'], $form_values['limit']);
	drupal_set_message (t('Limit saved succesfully'));
}

/**
	* Implementation of hook_node_type
	* Delete all rules related to the node type being deleted
	*/
function node_limitnumber_node_type ($op, $type) {
	if ($op == 'delete') {
		$q = "DELETE FROM {node_limitnumber_rules} WHERE class = 'role' AND pubtype = '%s'";
		$result = db_num_rows (db_query ($q, $type->type));
		if ($result > 0) {
			drupal_set_message (sprintf(t('%d limit rules deleted.'), $result));
		}
	}
}
/**
	* Saving the modifications on existant rules
	*/
function node_limitnumber_admin_modify_form_submit ($id, $values) {
	$rows = $values['modify'];
	foreach ($rows as $row) {
		$q = "UPDATE {node_limitnumber_rules} SET item = %d, pubtype = '%s', max = %d WHERE id = %d";
		db_query ($q, $row['role'], $row['pubtype'], $row['limit'], $row['id']);
	}
	drupal_set_message (t('Limits updated'));
}

/**
	* Implementation of hook_nodeapi
	* This is where the real job gets done.
	*/
function node_limitnumber_nodeapi ($node, $op) {
	include_once ('node_limitnumber.inc');
	global $user;

	if (!$node->nid) {
		$account = $user;
	} else {
		$account = user_load(array('uid' => $node->uid));
	}

	if (($op == 'prepare' || $op == 'submit')) {
		$limit = _get_limits_for_user ($account, $node->type);

		if (!empty($limit)) { // We get the total number of nodes of this type owned by this user

			$type_name = node_get_types('name', $node);

			$q = "SELECT * FROM {node} WHERE type = '%s' AND uid = %d";


			$count = db_num_rows(db_query( $q, $node->type, $account->uid));

			// We have the data, now we check the limit
			if (!$node->nid && $user->uid != 1) {
				// This is an attempt to create a new post
				if ($count >= $limit) {
					drupal_set_message (t("You have reached the maximum of !limit number of @type items you may post.", array ('!limit' => $limit, '@type' => $type_name)), 'error');

				drupal_goto ('node/add');
				} else {
					drupal_set_message (t("This is item !count out of a maximum number of !limit of @type items you may post.", array ('!count' => $count+1, '!limit' => $limit, '@type' => $type_name)),NULL,false);
				}
			} else {
				if($user->uid == $account->uid) {
					// The user is editing an existing post
					if ($count >= $limit) {
						drupal_set_message ('
<p>' . t("You have reached the maximum of !limit number of @type items you may post.", array ('!limit' => $limit, '@type' => $type_name)) .
  '</p>
<p>' . t('You can\'t create any more items of this type, but you can edit this item.') . '</p>
' );
					} else {
						drupal_set_message (t("You have created !count out of a maximum number of !limit of @type items you may post.", array ('!count' => $count, '!limit' => $limit, '@type' => $type_name)));
					}
				} else {
					// The user is editing someone else's exisiting post
					if ($count >= $limit) {
						drupal_set_message (t("@name has reached the maximum of !limit number of @type items.", array ('@name' => $account->name, '!limit' => $limit, '@type' => $type_name)));
					} else {
						drupal_set_message (t("@name has created !count out of a maximum number of !limit of @type items.", array ('@name' => $account->name, '!count' => $count, '!limit' => $limit, '@type' => $type_name)));
					}
				}
			}
		}
	}
}