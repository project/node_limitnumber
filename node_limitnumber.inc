<?php


/**
	* Helper functions for node_limitnumber.module
	*/


/**
	* Admin form for new rules
	*/

function node_limitnumber_admin_add_form () {
	// Get list of node types and roles
	$roles = user_roles (true);
	$pubtypes = node_get_types('names');
	// Build the form
	$form = array(
		'#id' => 'node_limitnumber_admin_add_form',
		'#redirect' => 'admin/settings/node_limitnumber',
	);
	$form['add'] = array (
		'#type' => 'fieldset'
	);
	$form['add']['title'] = array (
		'#value' => '<h2>'.t('Add limit').'</h2>',
	);
	$form['add']['role'] = array (
		'#type' => 'select',
		'#required' => true,
		'#title' => t('Role'),
		'#options' => $roles
	);
	$form['add']['pubtype'] = array (
		'#type' => 'select',
		'#required' => true,
		'#title' => t('Publication type'),
		'#options' => $pubtypes
	);
	$form['add']['limit'] = array (
		'#type' => 'textfield',
		'#required' => true,
		'#title' => t('Limit'),
		'#description' => t('Number of nodes of selected type the user is allowed to send. A limit of zero is not allowed, you\'d better use !access_control for that.', array ( '!access_control' => l(t('Access control'), 'admin/user/access'))),
		'#maxlength' => 4,
		'#size' => 4,
		'#validate' => array('node_limitnumber_validate_limit' => array()),
	);
	$form['add']['submit'] = array (
		'#type' => 'submit',
		'#value' => t('Add')
	);
	return $form;
}

function node_limitnumber_admin_modify_form () {
	// Get list of node types and roles, and actual rules
	$roles = user_roles (true);
	$pubtypes = node_get_types('names');
	$rules = node_limitnumber_get_all();
	// Build the form
	$form = array(
		'#id' => 'node_limitnumber_admin_modify_form',
		'#redirect' => 'admin/settings/node_limitnumber',
	);
	$form['modify'] = array (
		'#type' => 'fieldset',
		'#tree' => true,
	);
	$form['modify']['title'] = array (
		'#value' => '<h2>'.t('Stored limits').'</h2>',
	);
	// Loop for every stored rule
	$i = 0;
	foreach ($rules as $rule) {
		$form['modify'][$i]['role'] = array (
			'#type' => 'select',
			'#required' => true,
			'#options' => $roles,
			'#default_value' => $rule['item'],
		);
		$form['modify'][$i]['pubtype'] = array (
			'#type' => 'select',
			'#required' => true,
			'#options' => $pubtypes,
			'#default_value' => $rule['pubtype'],
		);
		$form['modify'][$i]['limit'] = array (
			'#type' => 'textfield',
			'#required' => true,
			'#maxlength' => 4,
			'#size' => 4,
			'#default_value' => $rule['max'],
			'#validate' => array('node_limitnumber_validate_limit' => array()),
		);
		$form['modify'][$i]['id'] = array (
			'#type' => 'hidden',
			'#value' => $rule['id'],
		);
		$i++;	
	}
	$form['modify']['submit'] = array (
		'#type' => 'submit',
		'#value' => t('Modify'),
		'#weight' => 99,
		'#tree' => false,
	);	
	return $form;
}
/**
	* Theme function to render modify rules form in admin side
	*/
function theme_node_limitnumber_admin_modify_form ($form) {
	$header = array (t('Role'), t('Publication type'), t('Limit'), '');
	$i = 0;
	while ($form['modify'][$i]) {
		$row = array (
			drupal_render($form['modify'][$i]['role']),
			drupal_render($form['modify'][$i]['pubtype']),
			drupal_render($form['modify'][$i]['limit']),
			drupal_render($form['modify'][$i]['id']) . l(t('delete'), 'admin/settings/node_limitnumber/delete/'.$form['modify'][$i]['id']['#value']),
		);
		unset ($form['modify'][$i]);
		$rows[] = $row;
		$i++;
	}
	$table = theme('table', $header, $rows);
	$form['modify']['table'] = array (
		'#value' => $table,
		'#weight' => 90,
	);
	$output = drupal_render_form($form['#id'], $form);
	return $output;
}
/**
	* Get all rules and return as multidimensional array.
	*/
function node_limitnumber_get_all () {
	$result = db_query ("SELECT * FROM {node_limitnumber_rules} WHERE class = 'role'");
	$rules = array ();
	while ($rules[] = db_fetch_array($result)) {
	}
	array_pop ($rules);
	return $rules;
}
/**
	* GHelper function: get all limits for a user and a publication type
	*/
function _get_limits_for_user ($user, $nodetype) {
	$i = count($user->roles);
	foreach ($user->roles as $key => $role) {
		$ax .= 'item = '. $key;
		$i--;
		if ($i > 0) $ax .= ' OR ';
	}
	$q = "SELECT max FROM {node_limitnumber_rules} WHERE class = 'role'";
	$q .= " AND pubtype = '".$nodetype."'";
	$q .= " AND (".$ax.") ORDER BY max DESC LIMIT 1";
	$result = db_query ($q);
	$max = db_result($result);
	return $max;
}