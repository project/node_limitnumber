<?php


$abstract = t("This module allows admin to limit the number of nodes of a given type a role can create. Different roles may have different limits.
	 <p>Example: Registered users can create all blog entries they want, but just one photo gallery with ten images. </p>
");